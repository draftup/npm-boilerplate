# NPM Boilerplate

## GitLab CI

Uncomment `release` stage to start automatic publishing your NPM library via CI.

Also you should provide `NPM_TOKEN` environment variable to make it possible to publish your library to NPM.

## Scoped npm package

To publish scoped npm package (`@scope/package-name`) add the following lines to `package.json`:

```json
  "publishConfig": {
    "access": "public"
  },
```
